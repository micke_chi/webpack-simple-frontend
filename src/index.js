import './app.scss';

import Vue from 'vue';

//Plugins
import VueDraggableV1 from 'vue-draggable';
Vue.use(VueDraggableV1);

//Componentes
Vue.component('dragdrop', require('./DragDrop.vue').default);
Vue.component('columna-component', require('./ColumnaComponent.vue').default);

var app = new Vue({
    el: '#app',
});

